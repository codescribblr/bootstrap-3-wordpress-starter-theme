<?php
/* Welcome to Showcase :)
This is the core Showcase file where most of the
main functions & features reside. If you have
any custom functions, it's best to put them
in the functions.php file.

Developed by: Jon Wadsworth
URL: http://codescribblr.com/
*/

/*********************
LAUNCH SHOWCASE
Let's fire off all the functions
and tools. I put it up here so it's
right up top and clean.
*********************/

// we're firing all out initial functions at the start
add_action( 'after_setup_theme', 'showcase_ahoy', 16 );

function showcase_ahoy() {

    // launching operation cleanup
    add_action( 'init', 'showcase_head_cleanup' );
    // remove WP version from RSS
    add_filter( 'the_generator', 'showcase_rss_version' );
    // remove pesky injected css for recent comments widget
    add_filter( 'wp_head', 'showcase_remove_wp_widget_recent_comments_style', 1 );
    // clean up comment styles in the head
    add_action( 'wp_head', 'showcase_remove_recent_comments_style', 1 );
    // clean up gallery output in wp
    add_filter( 'gallery_style', 'showcase_gallery_style' );

    // enqueue base scripts and styles
    add_action( 'wp_enqueue_scripts', 'showcase_scripts_and_styles', 999 );
    // ie conditional wrapper

    // launching this stuff after theme setup
    showcase_theme_support();

    // adding sidebars to Wordpress (these are created in functions.php)
    add_action( 'widgets_init', 'showcase_register_sidebars' );
    // adding the showcase search form (created in functions.php)
    //add_filter( 'get_search_form', 'showcase_wpsearch' ); //this will override searchform.php

    // cleaning up random code around images
    add_filter( 'the_content', 'showcase_filter_ptags_on_images' );
    // cleaning up excerpt
    add_filter( 'excerpt_more', 'showcase_excerpt_more' );

} /* end showcase ahoy */

/*********************
WP_HEAD GOODNESS
The default wordpress head is
a mess. Let's clean it up by
removing all the junk we don't
need.
*********************/

function showcase_head_cleanup() {
	// category feeds
	// remove_action( 'wp_head', 'feed_links_extra', 3 );
	// post and comment feeds
	// remove_action( 'wp_head', 'feed_links', 2 );
	// EditURI link
	remove_action( 'wp_head', 'rsd_link' );
	// windows live writer
	remove_action( 'wp_head', 'wlwmanifest_link' );
	// index link
	remove_action( 'wp_head', 'index_rel_link' );
	// previous link
	remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
	// start link
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
	// links for adjacent posts
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
	// WP version
	remove_action( 'wp_head', 'wp_generator' );
	// remove WP version from css
	add_filter( 'style_loader_src', 'showcase_remove_wp_ver_css_js', 9999 );
	// remove Wp version from scripts
	add_filter( 'script_loader_src', 'showcase_remove_wp_ver_css_js', 9999 );

} /* end showcase head cleanup */

// remove WP version from RSS
function showcase_rss_version() { return ''; }

// remove WP version from scripts
function showcase_remove_wp_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}

// remove injected CSS for recent comments widget
function showcase_remove_wp_widget_recent_comments_style() {
   if ( has_filter( 'wp_head', 'wp_widget_recent_comments_style' ) ) {
      remove_filter( 'wp_head', 'wp_widget_recent_comments_style' );
   }
}

// remove injected CSS from recent comments widget
function showcase_remove_recent_comments_style() {
  global $wp_widget_factory;
  if (isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments'])) {
    remove_action( 'wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style') );
  }
}

// remove injected CSS from gallery
function showcase_gallery_style($css) {
  return preg_replace( "!<style type='text/css'>(.*?)</style>!s", '', $css );
}


/*********************
SCRIPTS & ENQUEUEING
*********************/

// loading modernizr and jquery, and reply script
function showcase_scripts_and_styles() {
  global $wp_styles; // call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way
  if (!is_admin()) {

    // modernizr (without media query polyfill)
    wp_register_script( 'showcase-modernizr', get_stylesheet_directory_uri() . '/library/js/libs/modernizr.custom.min.js', array(), '2.5.3', false );

    // ShadowBox styles
    wp_register_style( 'showcase-shadowbox', get_stylesheet_directory_uri() . '/library/css/libs/shadowbox/shadowbox.css', array(), '3.0.3' );

    // MMenu styles
    wp_register_style( 'showcase-mmenu', get_stylesheet_directory_uri() . '/library/css/libs/jquery.mmenu.all.css', array(), '4.5.3' );

    // Slidebars styles
    wp_register_style( 'showcase-slidebars', get_stylesheet_directory_uri() . '/library/css/libs/slidebars.css', array(), '4.1.6' );

    // Select2 styles
    wp_register_style( 'showcase-select2', get_stylesheet_directory_uri() . '/library/css/libs/select2.css', array(), '3.4.8' );

    // register main stylesheet
    wp_register_style( 'showcase-stylesheet', get_stylesheet_directory_uri() . '/library/less/style.css', array(), '', 'all' );

    // ie-only style sheet
    wp_register_style( 'showcase-ie-only', get_stylesheet_directory_uri() . '/library/css/ie.css', array(), '' );

    wp_register_style("jquery-ui-css", "//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/ui-lightness/jquery-ui.min.css", array(), '');

    // comment reply script for threaded comments
    if ( is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
		wp_enqueue_script( 'comment-reply' );
    }

    // Add this and the enqueue function below to add jquery validate
    wp_register_script( 'jquery-validate', '//ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.min.js', array( 'jquery' ), '1.13.0', true );

    //adding scripts file in the footer
    // Google Map API
    wp_register_script( 'google-map-api', '//maps.googleapis.com/maps/api/js?v=3.exp&sensor=false', array(), '3.0', true );

    // jQuery easing
    wp_register_script( 'jquery-easing', get_stylesheet_directory_uri().'/library/js/libs/jquery-easing-min.js', array( 'jquery' ), '1.3.0', true );

    // jQuery ddSlick select replacement
    wp_register_script( 'ddslick', get_stylesheet_directory_uri().'/library/js/libs/jquery.ddslick.min.js', array( 'jquery' ), '2.0', true );

    // select2 select replacement
    wp_register_script( 'select2', get_stylesheet_directory_uri().'/library/js/libs/select2.min.js', array( 'jquery' ), '3.4.8', true );

    // jQuery customSelect select replacement
    wp_register_script( 'customSelect', get_stylesheet_directory_uri().'/library/js/libs/jquery.customSelect.min.js', array( 'jquery' ), '0.5.1', true );

    // MMenu
    wp_register_script( 'mmenu', get_stylesheet_directory_uri().'/library/js/libs/jquery.mmenu.min.all.js', array( 'jquery' ), '4.5.3', true );

    // FitVids
    wp_register_script( 'fitvids', get_stylesheet_directory_uri().'/library/js/libs/jquery.fitvids.js', array( 'jquery' ), '1.1', true );

    // Slidebars
    wp_register_script( 'slidebars', get_stylesheet_directory_uri().'/library/js/libs/slidebars.js', array( 'jquery' ), '4.1.6', true );

    // LazyLoad
    wp_register_script( 'lazyload', get_stylesheet_directory_uri().'/library/js/libs/jquery.lazy.min.js', array( 'jquery' ), '0.1.11', true );

    // ShadowBox
    wp_register_script( 'shadowbox', get_stylesheet_directory_uri().'/library/js/libs/shadowbox.js', array( 'jquery' ), '3.0.3', true );

    // Cycle2
    wp_register_script( 'cycle2', get_stylesheet_directory_uri().'/library/js/libs/jquery.cycle2.min.js', array( 'jquery' ), '2.0', true );

    // Cycle2 Swipe Plugin
    wp_register_script( 'cycle2-swipe', get_stylesheet_directory_uri().'/library/js/libs/jquery.cycle2.swipe.min.js', array( 'jquery', 'cycle2' ), '2.0', true );

    // Cycle2 Carousel Plugin
    wp_register_script( 'cycle2-carousel', get_stylesheet_directory_uri().'/library/js/libs/jquery.cycle2.carousel.min.js', array( 'jquery', 'cycle2' ), '2.0', true );

    // ifvisible
    wp_register_script( 'ifvisible', get_stylesheet_directory_uri().'/library/js/libs/ifvisible.js', array( 'jquery' ), '1.6.2', true );

    // Bootstrap JS
    wp_register_script( 'showcase-bootstrap', get_stylesheet_directory_uri() . '/library/js/libs/bootstrap.min.js', array(), '3.0.0', true );

    //adding scripts file in the footer
    wp_register_script( 'showcase-js', get_stylesheet_directory_uri() . '/library/js/scripts.js', array( 'jquery' ), '', true );
    wp_localize_script( 'showcase-js', 'showcaseAjax', array('ajaxurl' => admin_url( 'admin-ajax.php' )));

    // enqueue styles and scripts
    wp_enqueue_script( 'showcase-modernizr' );
    //wp_enqueue_style( 'jquery-ui-css' );
    // wp_enqueue_style( 'showcase-shadowbox' );
    // wp_enqueue_style( 'showcase-mmenu' );
    //wp_enqueue_style( 'showcase-slidebars' );
    //wp_enqueue_style( 'showcase-select2' );
    wp_enqueue_style( 'showcase-stylesheet' );
    wp_enqueue_style( 'showcase-ie-only' );

    $wp_styles->add_data( 'showcase-ie-only', 'conditional', 'lt IE 9' ); // add conditional wrapper around ie stylesheet

    /*
    I recommend using a plugin to call jQuery
    using the google cdn. That way it stays cached
    and your site will load faster.
    */
    wp_enqueue_script( 'jquery' );
    // wp_enqueue_script( 'jquery-validate' );
    // wp_enqueue_script( 'google-map-api' );
    // wp_enqueue_script( 'jquery-easing' );
    // wp_enqueue_script( 'mmenu' );
    // wp_enqueue_script( 'slidebars' );
    // wp_enqueue_script( 'lazyload' );
    // wp_enqueue_script( 'shadowbox' );
    // wp_enqueue_script( 'cycle2' );
    // wp_enqueue_script( 'cycle2-swipe' );
    // wp_enqueue_script( 'cycle2-carousel' );
    // wp_enqueue_script( 'ifvisible' );
    // wp_enqueue_script( 'ddslick' );
    // wp_enqueue_script( 'select2' );
    // wp_enqueue_script( 'customSelect' );
    wp_enqueue_script( 'showcase-js' );
    wp_enqueue_script( 'showcase-bootstrap' );
    //get rid of anyone else's attempt to add bootstrap default styles
    wp_deregister_style( 'bootstrap' );

  }
}

/*********************
THEME SUPPORT
*********************/

// Adding WP 3+ Functions & Theme Support
function showcase_theme_support() {

	// wp thumbnails (sizes handled in functions.php)
	add_theme_support( 'post-thumbnails' );

	// default thumb size
	//set_post_thumbnail_size(125, 125, true);

	// wp custom background (thx to @bransonwerner for update)
	add_theme_support( 'custom-background',
	    array(
	    'default-image' => '',  // background image default
	    'default-color' => '', // background color default (dont add the #)
	    'wp-head-callback' => '_custom_background_cb',
	    'admin-head-callback' => '',
	    'admin-preview-callback' => ''
	    )
	);

	// rss thingy
	add_theme_support('automatic-feed-links');

	// to add header image support go here: http://themble.com/support/adding-header-background-image-support/

	// adding post format support
	add_theme_support( 'post-formats',
		array(
			'aside',             // title less blurb
			'gallery',           // gallery of images
			'link',              // quick link to other site
			'image',             // an image
			'quote',             // a quick quote
			'status',            // a Facebook like status update
			'video',             // video
			'audio',             // audio
			'chat'               // chat transcript
		)
	);

	// wp menus
	add_theme_support( 'menus' );

	// registering wp3+ menus
	register_nav_menus(
		array(
			'main-nav' => __( 'The Main Menu', 'showcasetheme' )   // main nav in header
		)
	);
} /* end showcase theme support */


/*********************
MENUS & NAVIGATION
*********************/

// the main menu
function showcase_main_nav() {
	// display the wp3 menu if available      

    wp_nav_menu(array(
    	'container' => false,                           // remove nav container
    	'container_class' => '',			            // class of container (should you choose to use it)
    	'menu' => __( 'The Main Menu', 'showcasetheme' ),  // nav name
    	'menu_class' => 'nav navbar-nav',         			// adding custom nav class
    	'theme_location' => 'main-nav',                 // where it's located in the theme
    	'before' => '',                                 // before the menu
        'after' => '',                                  // after the menu
        'link_before' => '',                            // before each link
        'link_after' => '',                             // after each link
        'depth' => 2,                                   // limit the depth of the nav
    	'walker' => new Bootstrap_Walker_Nav_Menu()     // custom menu in navbar.php     
	));
} /* end showcase main nav */
function showcase_mobile_nav() {
    // display the wp3 menu if available      

    wp_nav_menu(array(
        'container' => false,                           // remove nav container
        'container_class' => '',                        // class of container (should you choose to use it)
        'menu' => __( 'The Main Menu', 'showcasetheme' ),  // nav name
        'menu_class' => 'nav',                         // adding custom nav class
        'theme_location' => 'main-nav',                 // where it's located in the theme
        'before' => '',                                 // before the menu
        'after' => '',                                  // after the menu
        'link_before' => '',                            // before each link
        'link_after' => '',                             // after each link
        'depth' => 2,                                   // limit the depth of the nav
        'walker' => new MMenu_Walker_Nav_Menu()     // custom menu in navbar.php     
    ));
} /* end showcase main nav */

// this is the fallback for header menu
function showcase_main_nav_fallback() {
	wp_page_menu( array(
		'show_home' => true,
    	'menu_class' => 'nav top-nav clearfix',      // adding custom nav class
		'include'     => '',
		'exclude'     => '',
		'echo'        => true,
        'link_before' => '',                            // before each link
        'link_after' => ''                             // after each link
	) );
}

// this is the fallback for footer menu
function showcase_footer_links_fallback() {
	/* you can put a default here if you like */
}

/*********************
RELATED POSTS FUNCTION
*********************/

// Related Posts Function (call using showcase_related_posts(); )
function showcase_related_posts() {
	echo '<ul id="showcase-related-posts">';
	global $post;
	$tags = wp_get_post_tags( $post->ID );
	if($tags) {
		foreach( $tags as $tag ) { 
			$tag_arr .= $tag->slug . ',';
		}
        $args = array(
        	'tag' => $tag_arr,
        	'numberposts' => 5, /* you can change this to show more */
        	'post__not_in' => array($post->ID)
     	);
        $related_posts = get_posts( $args );
        if($related_posts) {
        	foreach ( $related_posts as $post ) : setup_postdata( $post ); ?>
	           	<li class="related_post"><a class="entry-unrelated" href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>
	        <?php endforeach; }
	    else { ?>
            <?php echo '<li class="no_related_post">' . __( 'No Related Posts Yet!', 'showcasetheme' ) . '</li>'; ?>
		<?php }
	}
	wp_reset_query();
	echo '</ul>';
} /* end showcase related posts function */

/*********************
PAGE NAVI
*********************/

// Numeric Page Navi (built into the theme by default)
function showcase_page_navi() {
	global $wp_query;
	$bignum = 999999999;
	if ( $wp_query->max_num_pages <= 1 )
	return;
	
	echo '<nav class="pagination">';
	
		echo paginate_links( array(
			'base' 			=> str_replace( $bignum, '%#%', esc_url( get_pagenum_link($bignum) ) ),
			'format' 		=> '',
			'current' 		=> max( 1, get_query_var('paged') ),
			'total' 		=> $wp_query->max_num_pages,
			'prev_text' 	=> '&larr;',
			'next_text' 	=> '&rarr;',
			'type'			=> 'list',
			'end_size'		=> 3,
			'mid_size'		=> 3
		) );
	
	echo '</nav>';
} /* end page navi */

/*********************
RANDOM CLEANUP ITEMS
*********************/

// remove the p from around imgs (http://css-tricks.com/snippets/wordpress/remove-paragraph-tags-from-around-images/)
function showcase_filter_ptags_on_images($content){
   return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}

// This removes the annoying […] to a Read More link
function showcase_excerpt_more($more) {
	global $post;
	// edit here if you like
return '...  <a class="excerpt-read-more" href="'. get_permalink($post->ID) . '" title="'. __( 'Read', 'showcasetheme' ) . get_the_title($post->ID).'">'. __( 'Read more &raquo;', 'showcasetheme' ) .'</a>';
}

/*
 * This is a modified the_author_posts_link() which just returns the link.
 *
 * This is necessary to allow usage of the usual l10n process with printf().
 */
function showcase_get_the_author_posts_link() {
	global $authordata;
	if ( !is_object( $authordata ) )
		return false;
	$link = sprintf(
		'<a href="%1$s" title="%2$s" rel="author">%3$s</a>',
		get_author_posts_url( $authordata->ID, $authordata->user_nicename ),
		esc_attr( sprintf( __( 'Posts by %s' ), get_the_author() ) ), // No further l10n needed, core will take care of this one
		get_the_author()
	);
	return $link;
}

/*********************
ADD ADMIN SEPARATORS
*********************/

function admin_seperators() {
   echo '<style type="text/css">
   		#adminmenu li.wp-menu-separator {margin: 0;}
   		.admin-color-fresh #adminmenu li.wp-menu-separator {background: #444;}
   		.admin-color-midnight #adminmenu li.wp-menu-separator {background: #4a5258;}
   		.admin-color-light #adminmenu li.wp-menu-separator {background: #c2c2c2;}
   		.admin-color-blue #adminmenu li.wp-menu-separator {background: #3c85a0;}
   		.admin-color-coffee #adminmenu li.wp-menu-separator {background: #83766d;}
   		.admin-color-ectoplasm #adminmenu li.wp-menu-separator {background: #715d8d;}
   		.admin-color-ocean #adminmenu li.wp-menu-separator {background: #8ca8af;}
   		.admin-color-sunrise #adminmenu li.wp-menu-separator {background: #a43d39;}
         </style>';
}
add_action('admin_head', 'admin_seperators');