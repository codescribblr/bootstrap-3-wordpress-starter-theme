<?php

/*
Adds a padded separator between custom list items
Accepts any html element as a separator (defaults to bullet)
Useage:
	basic:	[separator]
	advanced: [separator separator="&pipe;"]
*/
add_shortcode('separator', 'showcase_padded_separator');
function showcase_padded_separator($atts) {
	extract(shortcode_atts(array(
		'separator' => '&bull;'
		), $atts));

	return '<span style="padding:0 20px">'.$separator.'</span>';
}

/*
Adds a standalone button using primary-btn styles
Useage:
	[button link="http://google.com"]Button Text[/button]
*/
add_shortcode('button', 'showcase_standalone_button');
function showcase_standalone_button($atts, $content) {
	extract(shortcode_atts(array(
		'link' => '#'
		), $atts));

	return '<div class="standalone-btn"><a class="btn btn-primary" href="'.$link.'">'.$content.'</a></div>';
}

/*
Adds a simple iframe
Useage:
	[iframe src="http://google.com" width="800" height="600"]
*/
add_shortcode('iframe', 'iframe');
function iframe($atts) {
   extract(shortcode_atts(array(
      'src' => "",
      'width' => "800",
      'height' => "600"
   ), $atts));

   $iframe = '<iframe src="'.$src.'" width="'.$width.'" height="'.$height.'" scrolling="no" allowtransparency="yes" frameborder="0" ></iframe>';
   
   return $iframe;
}