<!-- Headline with border wrap -->
<?php if($page_module['icon']):?>
<p class="introline-image"><img src="<?php echo $page_module['icon'];?>" width="70" height="70" /></p>
<?php endif;?>
<p class="introline"><span class="titleline start hidden-xs"></span><span class="headline"><?php echo $page_module['headline'];?></span><span class="titleline fin hidden-xs"></span></p>
<!-- /Headline with border wrap -->