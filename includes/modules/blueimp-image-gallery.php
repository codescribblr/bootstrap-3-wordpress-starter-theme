<!-- Image Gallery -->
<div class="row image_gallery">
	<?php $i = 1;?>
	<?php foreach($page_module['images'] as $image):?>
	<div class="col-md-3 col-sm-4 col-xs-6"><a href="<?php echo $image['url'];?>" data-gallery><img class="img-responsive" src="<?php the_thumb_src($image['url'], 280, 160);?>" /></a></div>
	<?php if($i==2):?>
	<div class="clearfix visible-xs"></div>
	<?php endif;?>
	<?php if($i==3):?>
	<div class="clearfix visible-sm"></div>
	<?php endif;?>
	<?php if($i<4){$i++;}else{$i=1;}?>
	<?php endforeach;?>
</div>
<!-- /Image Gallery -->
<!-- The Bootstrap Image Gallery lightbox -->
<div id="blueimp-gallery" class="blueimp-gallery">
    <!-- The container for the modal slides -->
    <div class="slides"></div>
    <!-- Controls for the borderless lightbox -->
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
    <!-- The modal dialog, which will be used to wrap the lightbox content -->
    <div class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body next"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left prev">
                        <i class="glyphicon glyphicon-chevron-left"></i>
                        Previous
                    </button>
                    <button type="button" class="btn btn-primary next">
                        Next
                        <i class="glyphicon glyphicon-chevron-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>