<?php if($page_modules = get_field('page_modules')):?>
<?php foreach($page_modules as $page_module):?>

<?php if($page_module['acf_fc_layout'] == 'page_title'):?>
<?php require_once('modules/page-title.php');?>

<?php elseif($page_module['acf_fc_layout'] == 'page_description'):?>
<?php require_once('modules/page-description.php');?>

<?php elseif($page_module['acf_fc_layout'] == 'standalone_button'):?>
<?php require_once('modules/standalone-button.php');?>

<?php elseif($page_module['acf_fc_layout'] == 'headline_with_border_wrap'):?>
<?php require_once('modules/headline-with-border-wrap.php');?>

<?php elseif($page_module['acf_fc_layout'] == 'full_width_content'):?>
<?php require_once('modules/full-width-content.php');?>

<?php elseif($page_module['acf_fc_layout'] == 'two_column_content'):?>
<?php require_once('modules/two-col-content.php');?>

<?php elseif($page_module['acf_fc_layout'] == 'three_column_content'):?>
<?php require_once('modules/three-col-content.php');?>

<?php elseif($page_module['acf_fc_layout'] == 'four_column_content'):?>
<?php require_once('modules/four-col-content.php');?>

<?php elseif($page_module['acf_fc_layout'] == 'image_gallery'):?>
<?php require_once('modules/blueimp-image-gallery.php');?>
<?php endif;?>

<?php endforeach;?>
<?php endif;?>