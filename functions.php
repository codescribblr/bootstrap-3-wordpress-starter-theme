<?php
/*
Author: Jon Wadsworth
URL: http://codescribblr.com/

This is where you can drop your custom functions or
just edit things like thumbnail sizes, header images,
sidebars, comments, ect.
*/

/************* INCLUDE NEEDED FILES ***************/

// library/Util.class.php  (basic utility functions for php)
require_once( 'library/Util.class.php' ); 

// library/navbar.php  (custom walker menu class for the Bootstrap menu)
require_once( 'library/navbar.php' ); // if you remove this the top navigation will break

/* library/builder.php (custom functions to add Bootstrap components and features)
	- numbered pagination
	- breadcrumbs
	- change tags to Bootstrap buttons
	- make read more link into Bootstrap button
	- add custom author fields to User profile page
*/
require_once( 'library/builder.php' ); // if you remove this a lot of things will break

/*
1. library/showcase.php
	- head cleanup (remove rsd, uri links, junk css, ect)
	- enqueueing scripts & styles
	- theme support functions
	- custom menu output & fallbacks
	- related post function
	- page-navi function
	- removing <p> from around images
	- customizing the post excerpt
	- custom google+ integration
	- adding custom fields to user profiles
*/
require_once( 'library/showcase.php' ); // if you remove this, showcase will break
/*
2. library/custom-post-type.php
	- an example custom post type
	- example custom taxonomy (like categories)
	- example custom taxonomy (like tags)
*/
require_once( 'library/custom-post-type.php' ); // you can disable this if you like
/*
3. library/admin.php
	- removing some default WordPress dashboard widgets
	- an example custom dashboard widget
	- adding custom login css
	- changing text in footer of admin
*/
require_once( 'library/admin.php' ); // this comes turned off by default
/*
4. library/video-functions.php
	- adding functions for videos
*/
require_once( 'library/video-functions.php' );
/*
5. library/image-functions.php
	- adding functions for image manipulation
*/
require_once( 'library/image-functions.php' );
/*
6. library/shortcodes.php
	- adding simple shortcodes to the theme
*/
require_once( 'library/shortcodes.php');
/*
7. library/ajax-functions.php
	- adding ajax functions to hook into for front-end ajax
*/
require_once( 'library/ajax-functions.php');

/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function showcase_register_sidebars() {
	register_sidebar(array(
		'id' => 'sidebar1',
		'name' => __( 'Sidebar 1', 'showcasetheme' ),
		'description' => __( 'The first (primary) sidebar.', 'showcasetheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

} // don't remove this bracket!

/************* COMMENT LAYOUT *********************/

// Comment Layout
function showcase_comments( $comment, $args, $depth ) {
   $GLOBALS['comment'] = $comment; ?>
	<li <?php comment_class(); ?>>
		<article id="comment-<?php comment_ID(); ?>" class="clearfix">
			<header class="comment-author vcard">
				<?php
				/*
					this is the new responsive optimized comment image. It used the new HTML5 data-attribute to display comment gravatars on larger screens only. What this means is that on larger posts, mobile sites don't have a ton of requests for comment images. This makes load time incredibly fast! If you'd like to change it back, just replace it with the regular wordpress gravatar call:
					echo get_avatar($comment,$size='32',$default='<path_to_url>' );
				*/
				?>
				<!-- custom gravatar call -->
				<?php
					// create variable
					$bgauthemail = get_comment_author_email();
				?>
				<img data-gravatar="http://www.gravatar.com/avatar/<?php echo md5( $bgauthemail ); ?>?s=32" class="load-gravatar avatar avatar-48 photo" height="32" width="32" src="<?php echo get_template_directory_uri(); ?>/library/images/nothing.gif" />
				<!-- end custom gravatar call -->
				<?php printf(__( '<cite class="fn">%s</cite>', 'showcasetheme' ), get_comment_author_link()) ?>
				<time datetime="<?php echo comment_time('Y-m-j'); ?>"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php comment_time(__( 'F jS, Y', 'showcasetheme' )); ?> </a></time>
				<?php edit_comment_link(__( '(Edit)', 'showcasetheme' ),'  ','') ?>
			</header>
			<?php if ($comment->comment_approved == '0') : ?>
				<div class="alert alert-info">
					<p><?php _e( 'Your comment is awaiting moderation.', 'showcasetheme' ) ?></p>
				</div>
			<?php endif; ?>
			<section class="comment_content clearfix">
				<?php comment_text() ?>
			</section>
			<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
		</article>
	<!-- </li> is added by WordPress automatically -->
<?php
} // don't remove this bracket!


// //Compile LESS into library/css/style.css using LESSC PHP Compiler  http://leafo.net/lessphp/ AND https://github.com/leafo/lessphp
require_once('library/less/lessc.inc.php');
$less = new lessc;
$less->compileFile(get_stylesheet_directory().'/library/less/style.less', get_stylesheet_directory().'/library/less/style.css');
$less->compileFile(get_stylesheet_directory().'/library/less/login.less', get_stylesheet_directory().'/library/less/login.css');
// TO DO
/*


*/